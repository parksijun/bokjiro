from bs4 import BeautifulSoup # 크롤링 BeautifulSoup 사용 
from module import dbModule # db사용
from apscheduler.schedulers.background import BackgroundScheduler # 스케줄러 사용
from apscheduler.jobstores.base import JobLookupError # error
from module.logger import logger # log 사용
from datetime import datetime
from pytz import timezone # timezone 설정 사용
import requests
import time

import bokjiroDBApi

# 복지로 크롤링.
class Scheduler:
    def __init__(self): # 클래스 생성시 Scheduler 데몬 생성.
        self.sched = BackgroundScheduler()
        self.sched.start()
        
    def __del__(self): # 클래스가 종료될때. 모든 job을 종료.
        self.shutdown()
    
    def shutdown(self): # 모든 job을 종료시키는 함수.
        self.sched.shutdown()
    
    def bokjiroScheduler(self):
        try:
            # cron 사용
            self.sched.add_job(self.bokjiro,'cron', minute="00", id="bokjifo_scheduler_categorie6", args=["6"]) # 노인  
            self.sched.add_job(self.bokjiro,'cron', minute="15", id="bokjifo_scheduler_categorie7", args=["7"]) # 장애인
            self.sched.add_job(self.bokjiro,'cron', minute="30", id="bokjifo_scheduler_categorie8", args=["8"]) # 한부모 ( 결손가정 )
            self.sched.add_job(self.bokjiro,'cron', minute="45", id="bokjifo_scheduler_categorie9", args=["9"]) # 다문화
            
            logger.debug("success to start Scheduler")
        
        except JobLookupError as err:
            logger.debug("fail to start Scheduler: {err}".format(err=err))
            return
        
    def bokjiro(self,categorie):
    
        #now = datetime.now()
        utcdate = datetime.now(timezone('UTC')).astimezone(timezone('Asia/Seoul'))
        nowDate = utcdate.strftime('%Y-%m-%d')
        #setDate = datetime(2020,7,20)
        pageUnit = 100 # 출력 개수
        
        result = []
        url = "http://bokjiro.go.kr/nwel/welfareinfo/livwelnews/news/retireveNewsList.do?stDate={}&endDate={}&pageUnit={}&tmpField1=0{}&tmpField1=10&tmpField1=15".format(nowDate, nowDate, pageUnit, categorie)
        html = requests.get(url).text
        
        bs_html = BeautifulSoup(html,"html.parser")
        
        news = bs_html.select(" .list_thumb .noneThumb")
        
        if categorie == '6':
            categorie = 1 # 노인
            categorie_name = "노년"
                
        elif categorie == '7':
            categorie = 2 # 장애인
            categorie_name = "장애인"
        
        elif categorie == '8':
            categorie = 3 # 한부모 ( 결손가정 )
            categorie_name = "한부모"
        
        elif categorie == '9':
            categorie = 4 # 다문화
            categorie_name = "다문화"
        
        if not news:
            logger.debug(categorie_name+" 업데이트 뉴스가 없습니다.")
            # DB crawling history 테이블 insert - 2020/10/26 15분마다 crawling 하여 insert 시 history 남김.
            db_class= dbModule.Database()
            val_cl_history = (categorie, 0)
            sql_cl_history = "INSERT INTO bokjiro_crawling_history(CATEGORIE,UPDATE_OPTION) VALUES(%s, %s)"
            try:
                db_class.execute(sql_cl_history, val_cl_history)
                db_class.commit()
            except:
                db_class.rollback()
                logger.debug("failed to insert into DataBase bokjiro_crawling_history")
                
            db_class.close()
        
        
        if news :
            # 제목,날짜,내용 읽어오기.            
            for each_line in news:        
                    title = each_line.select_one(".tit").a.get_text().strip()             # 제목
                    news_date = each_line.select_one(" .date").span.get_text().strip()    # 날짜
                    
                    ##################################################################################################
                    contents = each_line.select_one(" .desc").find("a")["href"]           # <a href = >
                    contentsHtml = requests.get("http://bokjiro.go.kr"+contents).text     # <a href = > read() 내용
                    
                    content_html = BeautifulSoup(contentsHtml,"html.parser")
                    newsContent = content_html.select(" .contents p")                     # 본문 내용 부분만 발췌
                    
                    content = []
                    for contents in newsContent:
                        if newsContent:
                            content.append(contents.get_text())
        
                    news_content = ''.join(content)
                    ##################################################################################################
                    
                    
                    result.append({"categorie" : categorie , "title" : title , "news_date" : news_date, "content": news_content})
            
            # DB connection
            db_class= dbModule.Database()
            sql = "SELECT * FROM bokjiroNews"
            db_class.execute(sql)
            
            db_result = db_class.cursor.fetchall()
            
            cnt = result.__len__()      # bokjiro 뉴스 크롤링 개수
            insert_cnt = 0              # insert 되어진 개수 값 변수
            
            result = sorted(result, key=lambda re: re['news_date'], reverse=False) # 크롤링 정보 뉴스 날짜 기준 오래된 순으로 sort
            
            # bokjiroNews 테이블에 값이 없으면 클로링 정보 insert.
            if not db_result:
                for i in range(cnt):
                    val1 = (result[i]["categorie"],result[i]["title"],result[i]["content"],result[i]["news_date"])
                    
                    sql1 = "INSERT INTO bokjiroNews(CATEGORIE, TITLE, CONTENT, NEWS_DATE) VALUES(%s, %s, %s, %s)"
                    try:
                        db_class.execute(sql1, val1)
                        db_class.commit()
                        insert_cnt += 1 # insert 성공시 1씩 증가.
                    except:
                        db_class.rollback()
                        logger.debug("failed to insert into DataBase")
                    
                db_class.close()
                
                if insert_cnt == 0:
                    logger.debug(categorie_name+" 뉴스 업데이트  실패")
                else:
                    # DB crawling history 테이블 insert - 2020/10/26 15분마다 crawling 하여 insert 시 history 남김.
                    db_class= dbModule.Database()
                    val_cl_history = (categorie,1)
                    sql_cl_history = "INSERT INTO bokjiro_crawling_history(CATEGORIE,UPDATE_OPTION) VALUES(%s, %s)"
                    try:
                        db_class.execute(sql_cl_history, val_cl_history)
                        db_class.commit()
                    except:
                        db_class.rollback()
                        logger.debug("failed to insert into DataBase bokjiro_crawling_history")
                    
                    db_class.close()
                    
                    logger.debug(categorie_name+" 뉴스 업데이트 "+str(insert_cnt)+"개 성공")
                
            
            # bokjiroNews 테이블에 값이 있으면
            elif db_result:
                new_bokjiro_news = []
                # bokjiroNews 테이블에 크롤링한 CATEGORIE, TITLE, NEWS_DATE 값으로 조회.
                for i in range(cnt):
                    compareVal = (result[i]["categorie"],result[i]["title"],result[i]["news_date"])
                    
                    compareSql = "select * "\
                                  "from bokjiroNews "\
                                  "where CATEGORIE = %s and TITLE = %s and NEWS_DATE = %s"
                    db_class.execute(compareSql, compareVal)
                    bokjiroDB_result = db_class.cursor.fetchall()
                    
                    # 조회한 결과값이 없는 경우( 업데이트 뉴스가 있는경우 new_bokjiro_news배열에 append )
                    if not bokjiroDB_result:
                        new_bokjiro_news.append({"categorie":result[i]["categorie"],"title":result[i]["title"],"news_date":result[i]["news_date"],"content":result[i]["content"]})
                                
                # 업데이트 뉴스가 있는 경우
                if new_bokjiro_news:
                    for i in range(new_bokjiro_news.__len__()):
                        val3 = (new_bokjiro_news[i]["categorie"],new_bokjiro_news[i]["title"],new_bokjiro_news[i]["content"],new_bokjiro_news[i]["news_date"])
                        
                        sql3 = "INSERT INTO bokjiroNews(CATEGORIE, TITLE, CONTENT, NEWS_DATE) VALUES(%s, %s, %s, %s)"
                        try:
                            db_class.execute(sql3, val3)
                            db_class.commit()
                            insert_cnt += 1 # insert 성공시 1씩 증가.
                        except:
                            db_class.rollback()
                            logger.debug("failed to insert into DataBase")
                    
                    db_class.close()
                    
                    if insert_cnt == 0:
                        logger.debug(categorie_name+" 뉴스 업데이트 실패.")
                    else:
                        # DB crawling history 테이블 insert - 2020/10/26 15분마다 crawling 하여 insert 시 history 남김.
                        db_class= dbModule.Database()
                        val_cl_history = (categorie,1)
                        sql_cl_history = "INSERT INTO bokjiro_crawling_history(CATEGORIE,UPDATE_OPTION) VALUES(%s, %s)"
                        try:
                            db_class.execute(sql_cl_history, val_cl_history)
                            db_class.commit()
                        except:
                            db_class.rollback()
                            logger.debug("failed to insert into DataBase bokjiro_crawling_history")
                            
                        db_class.close()
                        logger.debug(categorie_name+" 뉴스 업데이트 "+str(insert_cnt)+"개 성공")
                
                # 업데이트 뉴스가 없는 경우        
                else:
                    logger.debug(categorie_name+" 업데이트 뉴스가 없습니다.")
                    # DB crawling history 테이블 insert - 2020/10/26 15분마다 crawling 하여 insert 시 history 남김.
                    val_cl_history = (categorie, 0)
                    sql_cl_history = "INSERT INTO bokjiro_crawling_history(CATEGORIE,UPDATE_OPTION) VALUES(%s, %s)"
                    try:
                        db_class.execute(sql_cl_history, val_cl_history)
                        db_class.commit()
                    except:
                        db_class.rollback()
                        logger.debug("failed to insert into DataBase bokjiro_crawling_history")
                    db_class.close()
                    
        # def bokjiro(categorie): 끝.        


if __name__ == '__main__':  
    
    Scheduler().bokjiroScheduler()
    bokjiroDBApi.app.run(host='0.0.0.0',port=5000,debug=False)
    #time.sleep(600)   
    
    '''
    while True:
        print("Running Scheduler.....")
        time.sleep(600)
    '''
    
    
    
    
    
    