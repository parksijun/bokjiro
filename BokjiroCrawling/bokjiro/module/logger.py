import logging # log를 위해 사용
import os
from logging.handlers import TimedRotatingFileHandler # 파일로그 자정이 되면 새로운 파일 생성을 위해 사용

logger = logging.getLogger(__name__)
formatter = logging.Formatter(u'%(asctime)s [%(levelname)8s] %(message)s')
logger.setLevel(logging.DEBUG)


# log 폴더 유무 확인
log_dir = './logs'

if not os.path.exists(log_dir):
    os.mkdir(log_dir)

fileHandler = TimedRotatingFileHandler(filename='./logs/bokjiro.log', when='midnight', interval=1, encoding='UTF-8')
fileHandler.setFormatter(formatter)
fileHandler.suffix = '%Y%m%d'
fileHandler.setLevel(logging.CRITICAL) # 에러 발생 시 로그 CRITICAL
fileHandler.setLevel(logging.DEBUG)
logger.addHandler(fileHandler)


streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
streamHandler.setLevel(logging.DEBUG)
logger.addHandler(streamHandler)



#fileHandler = TimedRotatingFileHandler(filename=log_dir+'/bokjiro.log', when='midnight', interval=1, encoding='UTF-8')



