from flask import Flask, Response, request
from flask.json import jsonify # return 값 json으로 출력 사용
from module import dbModule # db사용
from module.logger import logger # log 사용
import json

# flask 웹서버를 실행 합니다.
app = Flask(__name__)
#app.config['JSON_AS_ASCII'] = False
# 복지로 뉴스 테이블 api
@app.route('/bokjiro', methods = ['GET'])
def bokjiroDB():
    #userTypeCnt = request.args.getlist('userType')
    # userType 는 사용자 유형 파라미터.
    userType = request.args.get('userType')
    newsId = request.args.get('newsId')
    newsCnt = request.args.get('newsCnt')
    # userType = 1 (노년), userType = 2 (장애인), userType = 3 (한부모), userType = 4 (다문화)  
    
    if userType and userType == '1' or userType == '2' or userType == '3' or userType == '4':
        
        # DB connection
        db_class= dbModule.Database()
        
        if not newsId or newsId == '':
            sql = "select * from bokjiroNews where CATEGORIE = {} order by IDX desc limit 1".format(int(userType))
            db_class.execute(sql)
            db_result = db_class.cursor.fetchall()
            
            if db_result:
                newsId = db_result[0]['IDX']+1
            else:
                newsId = 0 
            
        if not newsCnt or newsCnt == '':
            newsCnt = 100
        
        # DB history 테이블 insert 2020/10/26
        val2 = (userType)
        sql2 = "INSERT INTO bokjiro_history(CATEGORIE) VALUES(%s)"
        try:
            db_class.execute(sql2, val2)
            db_class.commit()
        except:
            db_class.rollback()
            logger.debug("failed to insert into DataBase bokjiro_history")        
            
        apiSql = "select * from bokjiroNews where CATEGORIE = %s and IDX < %s "\
                "order by IDX desc limit %s"
        
        apiVal = (int(userType),int(newsId),int(newsCnt)) 
        
        db_class.execute(apiSql,apiVal)
        
        db_result = db_class.cursor.fetchall()
        
        db_class.close()
        
        bokjiro_db_result = []
        db_cnt = db_result.__len__()
        
        for i in range(db_cnt):
            
            db_result[i]["REG_DATE"] = db_result[i]["REG_DATE"].strftime("%Y-%m-%d %H:%M:%S") # db REG_DATE 문자열 변환
            bokjiro_db_result.append({"news_id":db_result[i]["IDX"],"title":db_result[i]["TITLE"],"content":db_result[i]["CONTENT"],"news_date":db_result[i]["NEWS_DATE"]})
            
        finally_result = {'result':bokjiro_db_result}
        return jsonify(finally_result)
        #json_str = json.dumps(finally_result, ensure_ascii=False)
        #return Response(json_str, mimetype='application/json')

    else:
        return "405 Method Not Allowed"
    
# 이 웹서버는 127.0.0.1 주소를 가지면 포트 5000번에 동작 
'''
if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=False)
'''