import pymysql
import configparser
import json
import os

class Database():
    
    def __init__(self):
        
        paasOs = os.environ.get('VCAP_SERVICES', 'None') # paaS-TA OS
        
        host = ''
        user = ''
        password = ''
        db = ''
        
        if paasOs != 'None' and paasOs != '':
            data = json.loads(paasOs)
            paasTaOs = {}
            for element in data['Mysql-DB']: # VCAP_SERVICES = {"Mysql-DB"= {[ ... ]}}
                paasTaOs = element['credentials']
            
            host = paasTaOs['hostname']
            user = paasTaOs['username']
            password = paasTaOs['password']
            db = paasTaOs['name']
            
        else: # paaS-TA os 가 아니라면 ConfigParser
            config = configparser.ConfigParser()
            config.read('dbconfig.ini')    
            
            host = config['MySQL']['host']
            user = config['MySQL']['user']
            password = config['MySQL']['password']
            db = config['MySQL']['db']
         
            
        self.db= pymysql.connect( host = host,
                                  user = user,
                                  password = password,
                                  db = db,
                                  charset = 'utf8',
                                )
        self.cursor= self.db.cursor(pymysql.cursors.DictCursor)
        
        
    def execute(self, query, args={}):
        self.cursor.execute(query, args) 
 
    def executeOne(self, query, args={}):
        self.cursor.execute(query, args)
        row= self.cursor.fetchone()
        return row
 
    def executeAll(self, query, args={}):
        self.cursor.execute(query, args)
        row= self.cursor.fetchall()
        return row
 
    def commit(self):
        self.db.commit()
        
    def rollback(self):
        self.db.rollback()
        
    def close(self):
        self.db.close()    
